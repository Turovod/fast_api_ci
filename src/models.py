from sqlalchemy import Column, Integer, String
from .database import Base


class Receipt(Base):
    __tablename__ = 'receipt'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True, nullable=False)
    cooking_time = Column(Integer, nullable=False)
    ingredients = Column(String, nullable=False)
    description = Column(String, nullable=False)
    views_count = Column(Integer, nullable=False, default=0)
