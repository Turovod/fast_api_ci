from pydantic import BaseModel, ConfigDict


class BaseReceipt(BaseModel):
    name: str
    cooking_time: int
    views_count: int


class ReceiptIn(BaseReceipt):
    description: str
    ingredients: str


class ReceiptOutShort(BaseReceipt):
    model_config = ConfigDict(from_attributes=True)
    id: int


class ReceiptOutDetail(BaseReceipt):
    model_config = ConfigDict(from_attributes=True)
    id: int
    description: str
    ingredients: str
