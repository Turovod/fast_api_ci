from typing import List
from fastapi import FastAPI
from sqlalchemy.future import select
from .models import Receipt
from .schemas import ReceiptIn, ReceiptOutDetail, ReceiptOutShort
from .database import engine, session, Base

app = FastAPI()


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await session.close()
    await engine.dispose()


@app.get('/receipts/', response_model=List[ReceiptOutShort])
async def receipts() -> List[Receipt]:
    result = await session.execute(select(Receipt).order_by(Receipt.views_count))
    return result.scalars().all()


@app.get('/receipts/{receipt_id}', response_model=ReceiptOutDetail)
async def receipts_by_id(receipt_id: int) -> Receipt:
    receipt = await session.get(Receipt, receipt_id)
    receipt.views_count += 1
    return receipt


@app.post('/receipts/', response_model=ReceiptOutDetail)
async def receipt(receipt_: ReceiptIn) -> Receipt:
    new_receipt = Receipt(**receipt_.model_dump())
    async with session.begin():
        print(new_receipt)
        session.add(new_receipt)
    return new_receipt
