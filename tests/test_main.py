from .conftest import client

def test_add_receipt():
    response = client.post('/receipts/', json={
        "name":"Яичница",
        "cooking_time": 10,
        "ingredients": "2 яйца",
        "description": "Утром",
        "views_count": 2,
    })
    print(response.json())
    assert response.status_code == 200

def test_get_receipt():
    response = client.get('/receipts/')
    print(response.json())
    assert response.status_code == 200